# PLEASE REMAKE THIS WITH REQUESTS I CANT TAKE IT ANYMORE ITS SO UNBELIEVABLY LONG

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
from selenium.webdriver.chrome.options import Options
import pyttsx3
import os

# SETUP
# pip install selenium
# pip install webdriver-manager
# pip install pyttsx3

novelScraped = [] # novel after scrapeNovel is run
novelCorrectLen = [[]] # novel min chapter length in sentences for user 
novelCorrectFormat = [] # novel in the correct format ready for TTS

title = input("Enter the full Light Novel URL from https://novelover.com/\n")
senLen = int(input("Enter minimum no of sentences in each mp3 (recommended 600 = ~25min or 0 to ignore)\n"))

# title = "https://novelover.com/novel/overlord-ln/"
def scrapeNovel():
    chrome_options = Options()
    chrome_options.add_argument("--headless")

    

    driver = webdriver.Chrome(ChromeDriverManager().install(),options=chrome_options)


    driver.get(title)
    # driver.maximize_window()

    time.sleep(5)
    chapterLinkHTML = driver.find_elements_by_xpath(
        "//li[@class='wp-manga-chapter  ']/a")
    chapterLinks = []
    for link in chapterLinkHTML:
        chapterLinks.append(link.get_attribute("href"))

    chapterLinks.reverse()
    print(len(chapterLinks))

    for chapter in chapterLinks:
        driver.get(chapter)
        time.sleep(5)
        paragraphArray = driver.find_elements_by_xpath(
            "//div[@class='text-left']/p")
        print("currrent url: ")
        print(chapter)
        print("current chapter: ",len(novelScraped)+1)
        print("number of sentences: ",len(paragraphArray))
        novelScraped.append([])
        for paragraph in paragraphArray:
            novelScraped[len(novelScraped)-1].append(paragraph.text)

def parseNovel():
    for n in novelScraped:
        # if the length of the latest entry in novel is less than senLen
        if(len(novelCorrectLen[len(novelCorrectLen)-1])<senLen):
            novelCorrectLen[len(novelCorrectLen)-1] = novelCorrectLen[len(novelCorrectLen)-1] + n
        else:
            novelCorrectLen.append(n)    
    for n in novelCorrectLen:
        novelCorrectFormat.append(" ".join(n))


def novelToTTS():
    engine = pyttsx3.init()
    # ADJUST THE PROPERTY BELLOW FOR DIFFERENT PLAYBACK SPEED
    engine.setProperty('rate', 300)     # setting up new voice rate
    voices = engine.getProperty('voices')  # get details of different voices
    engine.setProperty('voice', voices[0].id)
    novelTitle = title.replace("https://novelover.com/novel/","")
    novelTitle = novelTitle.replace("/","")
    counter = 0
    currentDirectory = os.getcwd()
    path = os.path.join(currentDirectory, novelTitle)
    os.mkdir(path)
    for n in novelCorrectFormat:
        counter+=1
        location = "./%s/%s-part-%d.mp3"%(novelTitle,novelTitle,counter)
        engine.save_to_file(n, location)
        engine.runAndWait()
        print("mp3 no ",counter," done!")

scrapeNovel()
parseNovel()
novelToTTS()
print("Completed!")
