import pyttsx3
import os
import re
import sys

# from google.cloud import texttospeech
# from gtts import gTTS

# for line in f:
#     print(line, end='')
# print(f.readlines())

# f = open("vol10c2.txt", "r", encoding="utf8")
# string = ""

# for line in f:
#     string += line
# # print(string)
# f.close()
# tts = gTTS(string)
# tts.save("vol10c2.mp3")
# print("Done!2")


engine = pyttsx3.init()

""" RATE"""
engine.setProperty('rate', 300)     # setting up new voice rate
voices = engine.getProperty('voices')  # get details of different voices
engine.setProperty('voice', voices[0].id)

# """VOLUME"""
# volume = engine.getProperty('volume')   #getting to know current volume level (min=0 and max=1)
# print (volume)                          #printing current volume level
# engine.setProperty('volume',1.0)    # setting up volume level  between 0 and 1

# f = open("egBook.txt", "r", encoding="utf8")
# string = ""
# for line in f:
#     string += line
# f.close()


# python3 ttsManual.py bookname1-volume & python3 ttsManual.py bookname2-volume

novelName = sys.argv[1]

for i in range(1,40):
    f = open("./{}/{}-{}.txt".format(novelName,novelName,i), "r", encoding="utf8")
    string = ""

    for line in f:
        string += line
    # print(string)

    string = string.replace("\f", " ")
    string = string.replace("https://mp4directs.com", "")
    string = string.replace("www.mp4directs.com", "")
    string = string.replace("mp4directs.com", "")
    string = string.replace("https://Jnovels.com", "")
    string = string.replace("Goldenagato", "")
    string = string.replace("|", " ")
    # string = re.sub("\n[0-9]+|Page [0-9]+|Page\|[0-9]+|Page  [0-9]+", " ", string)
    string = re.sub("\n[0-9]+\n|Page[0-9]+|Page\|[0-9]+|Page +[0-9]+", " ", string)
    string = re.sub("\.+", ".", string)
    string = re.sub("=+", "=", string)
    string = string.replace("\n", " ")
    string = string.replace("-", " ")
    string = string.replace("_", " ")
    string = string.replace("<", "")
    string = string.replace(">", "")
    string = string.replace("[", "")
    string = string.replace("]", "")
    string = string.replace("』", "")
    string = string.replace("『", "")
    string = string.replace("―", " ")
    string = string.replace("***", " ")
    string = string.replace("*", "")
    string = string.replace("…", "")
    string = string.replace("»", "")
    string = string.replace("«", "")
    string = string.replace("...", "")
    string = string.replace("✧", "")
    string = string.replace("◇", "")
    string = string.replace("◆", "")
    string = string.replace("■", "")
    string = string.replace("◊", "")
    string = string.replace("”", "”\n")
    f.close()
    # currentDirectory = os.getcwd()
    # path = os.path.join(currentDirectory, "test")
    # os.mkdir(path)

    # string = "HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO HELLO "
    # engine.say(string)
    engine.save_to_file(string, "./{}/{}-{}.mp3".format(novelName,novelName,i))
    engine.runAndWait()
    print("Done no:", i)
print("Finished!")
